import { byId } from 'src/utils';

import { updateById, InternalDispatchTypeError } from './utils';

function reducerEditingName(schema, state, type, action) {
	switch (type) {
		case '':
			return state.editingTermName === action.termId
				? state
				: {
					...state,
					editingTermName: action.termId,
				};
		case 'cancel':
			return state.editingTermName == null
				? state
				: {
					...state,
					editingTermName: null,
				};
		case 'confirm':
			return state.editingTermName == null
				? state
				: {
					...state,
					editingTermName: null,
					terms: state.terms[schema.id].find(byId(state.editingTermName)).name === action.payload
						? state.terms
						: {
							...state.terms,
							[schema.id]: updateById(state.terms[schema.id], state.editingTermName, { name: action.payload }),
						},
				};
		default:
			throw new InternalDispatchTypeError();
	}
}

export default reducerEditingName;
