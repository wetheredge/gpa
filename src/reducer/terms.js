import getId from 'src/getId';
import { byId, newTerm, not, termsToState } from 'src/utils';

import reducerFocus from './focus';
import reducerTermsClasses from './termsClasses';
import reducerEditName from './termsEditName';
import { InternalDispatchTypeError, splitType } from './utils';

function reducerTerms(schema, state, type, action) {
	const { payload } = action;
	const [mainType, subType] = splitType(type);

	switch (mainType) {
		case 'classes':
			return reducerTermsClasses(schema, state, subType, action);
		case 'delete':
			return reducerTermsDelete(schema, state, subType, action);
		case 'editName':
			return reducerEditName(schema, state, subType, action);

		case 'new': {
			const id = getId();

			const newState = {
				...state,
				terms: {
					...state.terms,
					[schema.id]: [...state.terms[schema.id], newTerm(schema, { id })],
				},
			};

			return newState;
		}

		case 'replace':
			return termsToState(
				schema,
				action.replaceAll ? payload : { ...state.terms, ...payload },
			);

		default:
			throw new InternalDispatchTypeError();
	}
}

function reducerTermsDelete(schema, state, type, { termId }) {
	switch (type) {
		case '':
			return state.delete === termId
				|| (!state.terms[schema.id].some(byId(termId)))
				? state
				: { ...state, delete: termId };
		case 'cancel':
			return state.delete == null
				? state
				: { ...state, delete: null };
		case 'confirm': {
			if (state.delete == null) {
				return state;
			}

			const newTerms = state.terms[schema.id].filter(byId(not(state.delete)));
			const deletedLastTerm = newTerms.length === 0;

			let newState = {
				...state,
				delete: null,
				terms: {
					...state.terms,
					[schema.id]: newTerms,
				},
			};

			if (deletedLastTerm) {
				newState = reducerTerms(schema, newState, 'new', {});
			} else if (newState.focus?.includes(state.delete) === true) {
				newState = reducerFocus(schema, newState, 'clear', {});
			}

			return newState;
		}

		default:
			throw new InternalDispatchTypeError();
	}
}

export default reducerTerms;
