import getId from 'src/getId';

import { testSchema } from './utils';

import getReducer from '.';

const reducer = getReducer(testSchema);

describe('new', () => {
	test('appends a new term without modifying any other terms', () => {
		const firstTerm = {
			classes: [],
			id: 'another-term',
			name: 'Another Term',
		};
		const state = {
			terms: {
				test: [firstTerm],
			},
		};

		const newState = reducer(state, {
			type: 'terms.new',
		});

		expect(newState.terms.test).toHaveLength(2);
		expect(newState.terms.test[0]).toBe(firstTerm);
	});

	test('applies default values', () => {
		const state = {
			terms: {
				test: [],
			},
		};
		const newState = reducer(state, {
			type: 'terms.new',
		});

		expect(newState.terms.test[0].name).toBe(testSchema.defaultTermName);
	});
});

describe('delete', () => {
	test.each([
		'cancel',
		'confirm',
	])('sets a term for deletion, then correctly %ss it', secondAction => {
		const firstTerm = { id: 'delete' };
		const secondTerm = { id: getId() };
		const state = {
			terms: {
				test: [firstTerm, secondTerm],
			},
		};

		const pendingState = reducer(state, {
			termId: 'delete',
			type: 'terms.delete',
		});

		expect(pendingState.delete).toBe('delete');

		const finalState = reducer(pendingState, {
			type: `terms.delete.${secondAction}`,
		});

		expect(finalState.delete).toBe(null);

		if (secondAction === 'confirm') {
			expect(finalState.terms.test).toStrictEqual([secondTerm]);
		} else {
			expect(finalState.terms.test).toStrictEqual([firstTerm, secondTerm]);
		}
	});

	test.each([
		'cancel',
		'confirm',
	])('avoids unnecessary rerender on %s if no term is pending deletion', action => {
		const state = {
			terms: [{
				id: getId(),
			}],
		};

		const newState = reducer(state, {
			type: `terms.delete.${action}`,
		});

		expect(newState).toBe(state);
	});

	test('avoids unnecessary rerender if term does not exist', () => {
		const state = {
			terms: {
				test: [{ id: getId() }],
			},
		};

		const newState = reducer(state, {
			termId: 'delete',
			type: 'terms.delete',
		});

		expect(newState).toBe(state);
	});

	test('avoids unnecessary rerender if term is already pending deletion', () => {
		const state = {
			terms: {
				test: [{ id: getId() }],
			},
		};

		const newState1 = reducer(state, {
			termId: getId(),
			type: 'terms.delete',
		});

		const newState2 = reducer(newState1, {
			termId: getId(),
			type: 'terms.delete',
		});

		expect(newState2).toBe(newState1);
	});

	test('adds a new term if the last one is deleted', () => {
		const id = 'delete';
		const state = {
			terms: {
				test: [{
					id,
				}],
			},
		};

		const pendingState = reducer(state, {
			termId: 'delete',
			type: 'terms.delete',
		});
		const finalState = reducer(pendingState, {
			type: 'terms.delete.confirm',
		});

		expect(finalState.terms.test).toHaveLength(1);
		expect(finalState.terms.test[0].id).toBe(getId());
	});
});

describe('replace', () => {
	test('replaceAll = false replaces only passed in terms, does not modify any others', () => {
		const other = [{ id: 'other-class' }];
		const newTerms = [{ id: 'new-class' }];

		const state = {
			delete: 'old',
			editingTermName: 'old',
			focus: 'old-field',
			terms: {
				other,
				test: [{ id: 'old-class' }],
			},
		};

		const newState = reducer(state, {
			payload: { test: newTerms },
			type: 'terms.replace',
		});

		expect(newState.terms.other).toBe(other);
		expect(newState.terms.test).toBe(newTerms);
	});

	test('replaceAll = true drops all terms that are not passed in', () => {
		const newTerms = [{ id: 'new-class' }];

		const state = {
			delete: 'old',
			editingTermName: 'old',
			focus: 'old-field',
			terms: {
				other: [{ id: 'other-class' }],
				test: [{ id: 'old-class' }],
			},
		};

		const newState = reducer(state, {
			payload: { test: newTerms },
			replaceAll: true,
			type: 'terms.replace',
		});

		expect(newState.terms).not.toContain('other');
		expect(newState.terms.test).toBe(newTerms);
	});

	test('clears delete and editingTermName, and focus', () => {
		const state = {
			delete: 'old',
			editingTermName: 'old',
			focus: 'old-field',
			terms: {
				test: [{
					id: 'old',
				}],
			},
		};

		const newState = reducer(state, {
			payload: {
				test: [{
					classes: [{
						id: 'new',
					}],
					id: 'new',
				}],
			},
			type: 'terms.replace',
		});

		expect(newState.delete).toBeNull();
		expect(newState.editingTermName).toBeNull();
		expect(newState.focus).toBeNull();
	});
});
