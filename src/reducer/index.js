import { debounce } from 'lodash';

import reducerFocus from './focus';
import reducerTerms from './terms';
import { DispatchTypeError, InternalDispatchTypeError, splitType } from './utils';

const updateLocalStorage = debounce(
	(oldState, newState) => {
		if (typeof window !== 'undefined') {
			for (const [key, value] of Object.entries(newState.terms)) {
				if (!Object.is(value, oldState.terms[key])) {
					localStorage.setItem(`term ${key}`, JSON.stringify(value));
				}
			}
		}
	},
	100,
	{ maxWait: 500 },
);

function getReducer(schema) {
	return (state, { type, ...action }) => {
		const [mainType, subType] = splitType(type);

		const newState = do {
			try {
				switch (mainType) {
					case 'focus':
						reducerFocus(schema, state, subType, action);
						break;
					case 'terms':
						reducerTerms(schema, state, subType, action);
						break;

					default:
						throw new InternalDispatchTypeError();
				}
			} catch (error) {
				throw error instanceof InternalDispatchTypeError
					? new DispatchTypeError(type)
					: error;
			}
		};

		updateLocalStorage(state, newState);
		return newState;
	};
}

export default getReducer;
