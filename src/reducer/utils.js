class DispatchTypeError extends Error {
	constructor(type, ...parameters) {
		super(...parameters);

		// Maintains proper stack trace (only available on V8)
		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, DispatchTypeError);
		}

		this.name = 'DispatchTypeError';
		this.message = `Invalid type passed to dispatch: ${type}`;
		this.type = type;
	}
}

class InternalDispatchTypeError extends Error {
	constructor(...parameters) {
		super(...parameters);

		// Maintains proper stack trace (only available on V8)
		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, InternalDispatchTypeError);
		}

		this.name = 'InternalDispatchTypeError';
	}
}

function constrain(x, { max, min, step }) {
	if (min != null && min > x) {
		return min;
	}

	if (max != null && max < x) {
		x = max;
	}

	const differenceFromStep = (x - min) % step;
	if (min != null && step != null && differenceFromStep > Number.EPSILON) {
		x = (x - differenceFromStep) + (step * Math.round(differenceFromStep / step));
	}

	return x;
}

function splitOnce(string, delimeter) {
	const [main, ...rest] = string.split(delimeter);
	return [main, rest.join(delimeter)];
}

const splitType = splitOnce(?, '.');

const testSchema = {
	defaultTermName: 'Test Term',
	grade: [
		{
			key: 'text',
			label: 'Text',
			type: 'text',
		},
		{
			key: 'number',
			label: 'Number',
			type: 'number',
		},
		{
			key: 'enum',
			label: 'Enum',
			type: 'enum',
			values: ['A', 'B'],
		},
	],
	id: 'test',
};

function updateById(all, id, change) {
	return all.map(x => {
		if (x.id === id) {
			change = typeof change === 'function'
				? change(x)
				: change;
			return { ...x, ...change };
		}

		return x;
	});
}

export {
	DispatchTypeError,
	InternalDispatchTypeError,
	constrain,
	splitOnce,
	splitType,
	testSchema,
	updateById,
};
