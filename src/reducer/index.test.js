import { testSchema } from './utils';

import getReducer from '.';

const reducer = getReducer(testSchema);

test.each([
	'invalid',
	'terms.invalid',
])("throws on invalid `type` and includes passed `type` in message (type = '%s')", type => {
	expect(() => reducer({}, { type })).toThrow(new RegExp(`^Invalid type passed to dispatch: ${type}`));
});
