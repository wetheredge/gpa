import getId from 'src/getId';
import { byId, byKey, newClass, not, typeOf } from 'src/utils';

import reducerFocus from './focus';
import { constrain, InternalDispatchTypeError, updateById } from './utils';

function reducerTermsClasses(schema, state, type, { classId, payload, termId, ...action }) {
	switch (type) {
		case 'new': {
			const id = getId();

			const newState = {
				...state,
				terms: {
					...state.terms,
					[schema.id]: updateById(state.terms[schema.id], termId, ({ classes }) => ({
						classes: [...classes, newClass(schema, { id })],
					})),
				},
			};

			return reducerFocus(schema, newState, 'set', { payload: `${schema.grade[0].key}-${id}${termId}` });
		}

		case 'update': {
			const thisSchema = schema.grade.find(byKey(action.key));

			if (!thisSchema) {
				throw new TypeError(`Invalid key passed to dispatch of type term.classes.update: ${action.key}`);
			}

			if (
				(thisSchema.type === 'enum' && !(thisSchema.values.includes(payload)))
				|| (thisSchema.type === 'number' && typeof payload !== 'number')
			) {
				throw new TypeError(`Invalid payload passed to term.classes.update for key '${thisSchema.key}' of type ${typeOf(payload)}: ${payload}`);
			}

			if (thisSchema.type === 'number') {
				payload = constrain(payload, thisSchema);
			}

			return state
				.terms[schema.id].find(byId(termId))
				.classes.find(byId(classId))[action.key] === payload
				? state
				: {
					...state,
					terms: {
						...state.terms,
						[schema.id]: updateById(state.terms[schema.id], termId, ({ classes }) => ({
							classes: updateById(classes, classId, {
								[action.key]: payload,
							}),
						})),
					},
				};
		}

		case 'delete': {
			const oldTerm = state.terms[schema.id].find(byId(termId));
			if (oldTerm?.classes?.some(byId(classId)) !== true) {
				return state;
			}

			const deletingLastClass = oldTerm.classes.length === 1;
			const newClassId = getId();

			let newState = {
				...state,
				terms: {
					...state.terms,
					[schema.id]: updateById(state.terms[schema.id], termId, ({ classes }) => {
						return {
							classes: deletingLastClass ? [newClass(schema, { id: newClassId })] : classes.filter(byId(not(classId))),
						};
					}),
				},
			};

			if (deletingLastClass) {
				newState = reducerFocus(schema, newState, 'set', { payload: `${schema.grade[0].key}-${newClassId}${termId}` });
			} else if (newState.focus?.includes(classId) === true) {
				newState = reducerFocus(schema, newState, 'clear', {});
			}

			return newState;
		}

		default:
			throw new InternalDispatchTypeError();
	}
}

export default reducerTermsClasses;
