import getId from 'src/getId';

import { testSchema } from './utils';

import getReducer from '.';

const reducer = getReducer(testSchema);

describe('set', () => {
	test('avoids unnecessary rerender if state is unchanged', () => {
		const state = {
			focus: getId(),
		};
		const newState = reducer(state, {
			payload: getId(),
			type: 'focus.set',
		});

		expect(newState).toBe(state);
	});

	test('sets state.focus', () => {
		const newState = reducer({}, {
			payload: getId(),
			type: 'focus.set',
		});

		expect(newState).toStrictEqual({ focus: getId() });
	});
});

describe('clear', () => {
	test('clears state.focus', () => {
		const newState = reducer({
			focus: 'some-id',
		}, {
			type: 'focus.clear',
		});

		expect(newState).toStrictEqual({ focus: null });
	});

	test('avoids unnecessary rerender if state.focus is already cleared', () => {
		const state = { focus: null };
		const newState = reducer(state, {
			type: 'focus.clear',
		});

		expect(newState).toBe(state);
	});
});
