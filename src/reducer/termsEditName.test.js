import { byId } from 'src/utils';

import { testSchema } from './utils';

import getReducer from '.';

const reducer = getReducer(testSchema);
const terms = {
	test: [{
		id: 'edit',
		name: 'Name',
	}],
};

test('sets state.editingTermName', () => {
	const state = { terms };

	const newState = reducer(state, {
		termId: 'edit',
		type: 'terms.editName',
	});

	expect(newState.editingTermName).toBe('edit');
});

test('avoids unnecessary rerender if editingTermName is already set', () => {
	const state = {
		editingTermName: 'edit',
		terms,
	};

	const newState = reducer(state, {
		termId: 'edit',
		type: 'terms.editName',
	});

	expect(newState).toBe(state);
});

test('avoids unnecessary rerender if term does not exist', () => {
	const state = {
		terms: {
			test: [],
		},
	};

	const newState = reducer(state, {
		termId: 'edit',
		type: 'terms.editName',
	});

	expect(newState.terms).toBe(state.terms);
});

describe.each([
	'cancel',
	'confirm',
])('%s', action => {
	test('avoids unnecessary rerender if no term name is being edited', () => {
		const state = { terms };

		const newState = reducer(state, {
			type: `terms.editName.${action}`,
		});

		expect(newState).toBe(state);
	});
});

describe('cancel', () => {
	test('clears state.editingTermName without changing terms', () => {
		const state = {
			editingTermName: 'edit',
			terms,
		};

		const newState = reducer(state, {
			type: 'terms.editName.cancel',
		});

		expect(newState.editingTermName).toBe(null);
		expect(newState.terms).toBe(state.terms);
	});
});

describe('confirm', () => {
	test('avoids unnecessary rerender if term name is not changed', () => {
		const state = {
			editingTermName: 'edit',
			terms,
		};

		const newState = reducer(state, {
			payload: 'Name',
			type: 'terms.editName.confirm',
		});

		expect(newState.terms).toBe(state.terms);
	});

	test('clears state.editingTermName and sets term name', () => {
		const state = {
			editingTermName: 'edit',
			terms,
		};

		const newState = reducer(state, {
			payload: 'New name',
			type: 'terms.editName.confirm',
		});

		expect(newState.editingTermName).toBe(null);
		expect(newState.terms.test.find(byId('edit')).name).toBe('New name');
	});
});
