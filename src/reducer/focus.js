import { InternalDispatchTypeError } from './utils';

function reducerFocus(_schema, state, type, { payload }) {
	switch (type) {
		case 'set':
			return state.focus === payload
				? state
				: { ...state, focus: payload };
		case 'clear':
			return state.focus == null
				? state
				: { ...state, focus: null };
		default:
			throw new InternalDispatchTypeError();
	}
}

export default reducerFocus;
