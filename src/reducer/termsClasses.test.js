import getId from 'src/getId';

import { testSchema } from './utils';

import getReducer from '.';

const reducer = getReducer(testSchema);

describe('new', () => {
	test('appends a new class without modifying any other classes or any other terms', () => {
		const firstTerm = {
			classes: [],
			id: 'another-term',
			name: 'Another Term',
		};
		const firstClass = {
			credits: 4,
			grade: 'A',
			id: 'another-class',
			name: 'Another class',
		};
		const state = {
			terms: {
				test: [firstTerm, {
					classes: [firstClass],
					id: getId(),
				}],
			},
		};

		const newState = reducer(state, {
			termId: getId(),
			type: 'terms.classes.new',
		});

		expect(newState.terms.test[0]).toBe(firstTerm);
		expect(newState.terms.test[1].classes[0]).toBe(firstClass);
	});

	test('applies default values', () => {
		const reducer = getReducer({
			...testSchema,
			grade: [
				{
					default: 2,
					key: 'number',
					label: 'Number with default',
					type: 'number',
				},
				{
					default: 'abc',
					key: 'text',
					label: 'Text with default',
					type: 'text',
				},
				{
					default: 'A',
					key: 'enum',
					label: 'Enum with default',
					type: 'enum',
					values: ['A', 'B'],
				},
			],
		});

		const state = {
			terms: {
				test: [],
			},
		};

		const newState = reducer(state, {
			type: 'terms.new',
		});

		expect(newState.terms.test[0].classes[0].number).toBe(2);
		expect(newState.terms.test[0].classes[0].text).toBe('abc');
		expect(newState.terms.test[0].classes[0].enum).toBe('A');
	});
});

describe('update', () => {
	test.each([
		['above max', 10, 7],
		['below min', 0, 3],
		['that are not a multiple of step', 4, 5],
		['that are not a multiple of step', 5.1, 5],
	])('normalizes numbers %s (%d -> %d)', (_name, payload, expected) => {
		const reducer = getReducer({
			...testSchema,
			grade: [{
				key: 'test',
				label: '',
				max: 7,
				min: 3,
				step: 2,
				type: 'number',
			}],
		});
		const state = {
			terms: {
				test: [{
					classes: [{ id: getId() }],
					id: getId(),
				}],
			},
		};

		const newState = reducer(state, {
			classId: getId(),
			key: 'test',
			payload,
			termId: getId(),
			type: 'terms.classes.update',
		});

		expect(newState.terms.test[0].classes[0].test).toBe(expected);
	});

	test('avoids unnecessary rerender if state is unchanged', () => {
		const state = {
			terms: {
				test: [{
					classes: [{
						id: getId(),
						number: 10,
					}],
					id: getId(),
				}],
			},
		};

		const newState = reducer(state, {
			classId: getId(),
			key: 'number',
			payload: 10,
			termId: getId(),
			type: 'terms.classes.update',
		});

		expect(newState).toBe(state);
	});

	test.each([
		'enum',
		'number',
	])('throws on invalid payload (%s)', key => {
		expect(() => reducer({}, {
			classId: getId(),
			key,
			payload: 'invalid',
			termId: getId(),
			type: 'terms.classes.update',
		})).toThrow('Invalid payload passed');
	});

	test('throws on invalid key', () => {
		expect(() => reducer({}, {
			key: 'invalid',
			type: 'terms.classes.update',
		})).toThrow('Invalid key passed');
	});
});

describe('delete', () => {
	test('deletes a class without modifying any other classes or any other terms', () => {
		const secondClass = {
			id: 'another-class',
		};
		const secondTerm = {
			id: 'another-term',
		};
		const state = {
			terms: {
				test: [{
					classes: [{
						id: 'delete',
					}, secondClass],
					id: 'delete',
				}, secondTerm],
			},
		};

		const newState = reducer(state, {
			classId: 'delete',
			termId: 'delete',
			type: 'terms.classes.delete',
		});

		expect(newState.terms.test[0].classes).toHaveLength(1);
		expect(newState.terms.test[0].classes[0]).toBe(secondClass);
		expect(newState.terms.test[1]).toBe(secondTerm);
	});

	test.each([
		'term',
		'class',
	])('avoids unnecessary rerender if %s does not exist', nonexistent => {
		const state = {
			terms: {
				test: nonexistent === 'term'
					? []
					: [{
						classes: [],
						id: 'delete',
					}],
			},
		};

		const newState = reducer(state, {
			classId: 'delete',
			termId: 'delete',
			type: 'terms.classes.delete',
		});

		expect(newState).toBe(state);
	});

	test('clears focus', () => {
		const state = {
			focus: 'name-delete',
			terms: {
				test: [{
					classes: [{
						id: 'term',
					}],
					id: 'class',
				}],
			},
		};

		const newState = reducer(state, {
			classId: 'class',
			termId: 'term',
			type: 'terms.classes.delete',
		});

		expect(newState.focus ?? '').not.toContain('class');
	});

	test('adds a new class if the last one is deleted and sets focus', () => {
		const state = {
			terms: {
				test: [{
					classes: [{
						id: 'delete',
					}],
					id: 'delete',
				}],
			},
		};

		const newState = reducer(state, {
			classId: 'delete',
			termId: 'delete',
			type: 'terms.classes.delete',
		});

		expect(newState.terms.test[0].classes).toHaveLength(1);
		expect(newState.terms.test[0].classes[0].id).not.toBe('delete');
		expect(newState.focus).not.toBe(getId());
		expect(newState.focus).toContain(getId());
	});
});
