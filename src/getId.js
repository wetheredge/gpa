import { customAlphabet } from 'nanoid';

const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
const length = 3;

const getId = customAlphabet(alphabet, length);

export default getId;
export {
	alphabet,
	length,
};
