const hexToRgb = hex => hex.match(/[\da-z]{2}/g).map(Number.parseInt(?, 16));
const rgbToHex = rgb => `#${rgb.map(x => Math.min(x, 255).toString(16).padStart(2, '0')).join('')}`;

const safeAreas = Object.fromEntries(
	['top', 'right', 'bottom', 'left'].map(side => [
		side,
		(fallback = '0px') => `env(safe-area-inset-${side}, ${fallback})`,
	]),
);

function getPaletteFromColors({ blue, green, orange, purple, red }) {
	return {
		error: { main: rgbToHex(red) },
		info: { main: rgbToHex(blue) },
		primary: { main: rgbToHex(purple) },
		secondary: { main: rgbToHex(blue) },
		success: { main: rgbToHex(green) },
		warning: { main: rgbToHex(orange) },
	};
}

function getGlassBackgroundColor(colorOrGray, themeKind) {
	const glassAlpha = 0.16;
	const backupAlpha = 0.95;
	const rgb = Array.isArray(colorOrGray) ? colorOrGray : (
		typeof colorOrGray === 'number'
			? Array.from({ length: 3 }).fill(colorOrGray)
			: hexToRgb(colorOrGray));
	const supportsBackdropFilter = typeof window === 'undefined' || CSS.supports('(backdrop-filter: none) or (-webkit-backdrop-filter: none)');

	if (supportsBackdropFilter) {
		return `rgb(${rgb.join(' ')} / ${glassAlpha})`;
	}

	const adjustedRgb = themeKind === 'dark'
		? rgb.map(x => Math.round(x * glassAlpha / backupAlpha))
		: rgb.map(x => Math.round(255 - ((255 - x) * glassAlpha / backupAlpha)));

	return `rgb(${adjustedRgb.join(' ')} / ${backupAlpha})`;
}

const glassBackdropFilter = 'saturate(180%) blur(20px)';

const baseTheme = {
	mixins: {
		glass: {
			backdropFilter: glassBackdropFilter,
			getBackgroundColor: getGlassBackgroundColor,
		},
		safeAreas,
	},
	overrides: {
		MuiAutocomplete: {
			popper: {
				backdropFilter: glassBackdropFilter,
			},
		},
		MuiCssBaseline: {
			'@global': {
				'input::-webkit-outer-spin-button, input::-webkit-inner-spin-button': {
					'-webkit-appearance': 'none',
					margin: 0,
				},
				'input[type=number]': {
					'-moz-appearance': 'textfield',
				},
			},
		},
		MuiDialog: {
			paper: {
				backdropFilter: `brightness(2) ${glassBackdropFilter}`,
			},
		},
		MuiPaper: {
			root: {
				backdropFilter: glassBackdropFilter,
			},
		},
	},
	props: Object.fromEntries([
		'FormControl',
		'NativeSelect',
		'Paper',
		'Select',
		'TextField',
	].map(name => [`Mui${name}`, {
		variant: 'outlined',
	}])),
	shadows: Array.from({ length: 25 }).fill('none'),
	typography: {
		fontFamily: [
			'-apple-system',
			'system-ui',
			'ui-sans-serif',
			'sans-serif',
		].join(),
	},
};

export {
	baseTheme,
	getGlassBackgroundColor,
	getPaletteFromColors,
	hexToRgb,
};
