import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import { merge } from 'lodash';

import { baseTheme, getPaletteFromColors, getGlassBackgroundColor } from './common';

const colors = {
	// In LCH: L=65%, C=65
	blue: [0, 169, 246], // H=252
	green: [0, 179, 149], // H=175
	orange: [242, 121, 76], // H=46
	purple: [191, 132, 251], // H=308
	red: [255, 104, 149], // H=5
	yellow: [180, 156, 18], // H=90
};

const theme = merge({}, baseTheme, {
	gpaPalette: [
		colors.purple,
		colors.blue,
		colors.green,
		colors.yellow,
		colors.orange,
		colors.red,
	],
	palette: {
		...getPaletteFromColors(colors),
		background: {
			default: '#010101',
			paper: getGlassBackgroundColor(100, 'dark'),
		},
		type: 'dark',
	},
});

const dark = responsiveFontSizes(createMuiTheme(theme));

export default dark;
