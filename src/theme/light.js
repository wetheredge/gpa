import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import { merge } from 'lodash';

import { baseTheme, getPaletteFromColors, getGlassBackgroundColor } from './common';

const colors = {
	// In LCH: L=48%, C=65
	blue: [0, 122, 180], // H=252
	green: [0, 130, 107], // H=175
	orange: [190, 77, 35], // H=46
	purple: [144, 88, 202], // H=308
	red: [207, 46, 108], // H=5
	yellow: [131, 113, 0], // H=90
};

const theme = merge({}, baseTheme, {
	gpaPalette: [
		colors.purple,
		colors.blue,
		colors.green,
		colors.yellow,
		colors.orange,
		colors.red,
	],
	palette: {
		...getPaletteFromColors(colors),
		background: {
			paper: getGlassBackgroundColor(255, 'light'),
		},
		type: 'light',
	},
});

const light = responsiveFontSizes(createMuiTheme(theme));

export default light;
