import getGpas from 'src/getGpas';

function getRoundedGpas(...args) {
	const round = x => Number.parseFloat(x.toFixed(3));
	const { final, terms } = getGpas(...args);
	return {
		final: round(final),
		terms: Object.fromEntries(
			Object.entries(terms)
				.map(([key, x]) => [key, round(x)]),
		),
	};
}

const gradeSchemas = {
	letter: {
		key: 'grade',
		mapping: { A: 4, B: 3, C: 2, D: 1, F: 0 },
		type: 'enum',
		values: ['A', 'B', 'C', 'D', 'F', 'W'],
	},
	percentage: { key: 'grade', type: 'number' },
};

describe('letter grades', () => {
	describe('with credits', () => {
		test('for 1 term', () => {
			const schema = {
				grade: [
					gradeSchemas.letter,
					{ key: 'credits', type: 'number' },
				],
			};
			const terms = [
				{
					classes: [
						{ credits: 4, grade: 'A' },
						{ credits: 2, grade: 'B' },
					],
					id: '1',
				},
			];

			const { final } = getRoundedGpas(schema, terms);

			expect(final).toBeCloseTo(3.667);
		});

		test('for 2 terms', () => {
			const schema = {
				grade: [
					gradeSchemas.letter,
					{ key: 'credits', type: 'number' },
				],
			};
			const terms = [
				{
					classes: [
						{ credits: 4, grade: 'A' },
						{ credits: 3, grade: 'B' },
					],
					id: '1',
				},
				{
					classes: [
						{ credits: 3, grade: 'C' },
						{ credits: 4, grade: 'D' },
					],
					id: '2',
				},
			];

			const result = getRoundedGpas(schema, terms);

			expect(result).toEqual({
				final: 2.5,
				terms: {
					1: 3.571,
					2: 1.429,
				},
			});
		});
	});
});

describe('percentage grades', () => {
	describe('with credits', () => {
		test('for 1 term', () => {
			const schema = {
				grade: [
					gradeSchemas.percentage,
					{ key: 'credits', type: 'number' },
				],
			};
			const terms = [
				{
					classes: [
						{ credits: 1, grade: 90 },
						{ credits: 0.5, grade: 99 },
					],
					id: '1',
				},
			];

			const { final } = getRoundedGpas(schema, terms);

			expect(final).toBe(93);
		});
	});

	describe('with multiplier', () => {
		test('without condition', () => {
			const schema = {
				grade: [
					gradeSchemas.percentage,
					{
						key: 'mult',
						purpose: 'multiplier',
						type: 'number',
					},
				],
			};
			const terms = [
				{
					classes: [
						{ grade: 100, mult: 1.5 },
					],
				},
			];

			const { final } = getRoundedGpas(schema, terms);

			expect(final).toBe(150);
		});

		test.each([
			['>', 51],
			['<', 49],
			['>=', 50],
			['>=', 51],
			['<=', 50],
			['<=', 49],
			['==', 50],
			['!=', 0],
		])('with met condition (%s %s)', (comparison, grade) => {
			const schema = {
				grade: [
					gradeSchemas.percentage,
					{
						condition: `grade ${comparison} 50`,
						key: 'mult',
						purpose: 'multiplier',
						type: 'number',
					},
				],
			};
			const terms = [
				{
					classes: [
						{ grade, mult: 2 },
					],
				},
			];

			const { final } = getRoundedGpas(schema, terms);

			expect(final).toBe(grade * 2);
		});

		test('with unmet condition', () => {
			const schema = {
				grade: [
					gradeSchemas.percentage,
					{
						condition: 'grade > 50',
						key: 'mult',
						purpose: 'multiplier',
						type: 'number',
					},
				],
			};
			const terms = [
				{
					classes: [
						{ grade: 40, mult: 1.5 },
					],
				},
			];

			const { final } = getRoundedGpas(schema, terms);

			expect(final).toBe(40);
		});
	});
});

describe.each([
	['letter', ['A', 'B'], 3.5],
	['percentage', [100, 90], 95],
])('%s grades', (gradeType, grades, target) => {
	describe('without credits', () => {
		test('for 1 term', () => {
			const schema = {
				grade: [
					gradeSchemas[gradeType],
				],
			};
			const terms = [
				{
					classes: grades.map(grade => ({ grade })),
					id: '1',
				},
			];

			const { final } = getRoundedGpas(schema, terms);
			expect(final).toBe(target);
		});
	});
});

describe.each([
	[
		'letter',
		{
			1: ['A', 'B'],
			2: ['C', 'D', 'F'],
		},
		{
			final: 2,
			terms: { 1: 3.5, 2: 1 },
		},
	],
	[
		'percentage',
		{
			1: [100, 90],
			2: [80, 70, 60],
		},
		{
			final: 80,
			terms: { 1: 95, 2: 70 },
		},
	],
])('%s grades', (gradeType, grades, target) => {
	describe('without credits', () => {
		test('for 2 terms', () => {
			const schema = {
				grade: [
					gradeSchemas[gradeType],
				],
			};
			const terms = Object.entries(grades).map(([id, termGrades]) => ({
				classes: termGrades.map(grade => ({ grade })),
				id,
			}));

			const result = getRoundedGpas(schema, terms);
			expect(result).toStrictEqual(target);
		});
	});
});

describe('incomplete data', () => {
	test('missing credits', () => {
		const schema = {
			grade: [
				gradeSchemas.percentage,
				{ key: 'credits', type: 'number' },
			],
		};
		const terms = [
			{
				classes: [
					{ credits: undefined, grade: 100 },
				],
				id: '1',
			},
		];

		const result = getRoundedGpas(schema, terms);

		expect(result.final).toBeNaN();
		expect(result.terms['1']).toBeUndefined();
	});

	test.each([
		['letter', ''],
		['percentage', undefined],
	])('missing %s grade', (gradeType, value) => {
		const schema = {
			grade: [
				gradeSchemas[gradeType],
			],
		};
		const terms = [
			{
				classes: [
					{ grade: value },
				],
				id: '1',
			},
		];

		const result = getRoundedGpas(schema, terms);

		expect(result.final).toBeNaN();
		expect(result.terms['1']).toBeUndefined();
	});

	test.each([
		['letter', '', 'A', 4],
		['percentage', undefined, 100, 100],
	])('missing 1 of 2 %s grades', (gradeType, missingValue, goodValue, target) => {
		const schema = {
			grade: [
				gradeSchemas[gradeType],
			],
		};
		const terms = [
			{
				classes: [
					{ grade: missingValue },
					{ grade: goodValue },
				],
				id: '1',
			},
		];

		const result = getRoundedGpas(schema, terms);

		expect(result.final).toBe(target);
		expect(result.terms['1']).toBe(target);
	});

	test.each([
		['letter', '', 'A', 4],
		['percentage', undefined, 100, 100],
	])('missing %s grade for 1 of 2 terms', (gradeType, missingValue, goodValue, target) => {
		const schema = {
			grade: [
				gradeSchemas[gradeType],
			],
		};
		const terms = [
			{
				classes: [
					{ grade: missingValue },
				],
				id: '1',
			},
			{
				classes: [
					{ grade: goodValue },
				],
				id: '2',
			},
		];

		const result = getRoundedGpas(schema, terms);

		expect(result.final).toBe(target);
		expect(result.terms['1']).toBeUndefined();
		expect(result.terms['2']).toBe(target);
	});
});
