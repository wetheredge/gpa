import PropTypes from 'prop-types';
import React from 'react';

import { useMediaQuery } from '@material-ui/core';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';

import { dark, light } from 'src/theme';

function ThemeProvider({ children }) {
	const systemLight = useMediaQuery('(prefers-color-scheme: light)');

	const theme = systemLight ? light : dark;

	return <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;
}

ThemeProvider.propTypes = {
	children: PropTypes.node.isRequired,
};

export default ThemeProvider;
