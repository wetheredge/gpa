import PropTypes from 'prop-types';
import React from 'react';

import { ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { DeleteTwoTone, EditTwoTone, SwapVertTwoTone } from '@material-ui/icons';

import { chainHandlers } from 'src/utils';

function TermMenu({ dispatch, onClose: handleClose, termId, ...props }) {
	const handleEditTermName = () => dispatch({ termId, type: 'terms.editName' });
	const handleDeleteTerm = () => dispatch({ termId, type: 'terms.delete' });

	return (
		<Menu
			anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
			transformOrigin={{ horizontal: 'right', vertical: 'top' }}
			onClose={handleClose}
			{...props}
		>
			<MenuItem onClick={chainHandlers(handleEditTermName, handleClose)}>
				<ListItemIcon>
					<EditTwoTone />
				</ListItemIcon>
				<Typography variant="inherit">Edit term name</Typography>
			</MenuItem>
			<MenuItem disabled>
				<ListItemIcon>
					<SwapVertTwoTone />
				</ListItemIcon>
				<Typography variant="inherit">Reorder terms</Typography>
			</MenuItem>
			<MenuItem onClick={chainHandlers(handleDeleteTerm, handleClose)}>
				<ListItemIcon>
					<DeleteTwoTone />
				</ListItemIcon>
				<Typography variant="inherit">Delete term</Typography>
			</MenuItem>
		</Menu>
	);
}

TermMenu.propTypes = {
	dispatch: PropTypes.func.isRequired,
	onClose: PropTypes.func.isRequired,
	termId: PropTypes.string.isRequired,
};

export default TermMenu;
