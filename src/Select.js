import PropTypes from 'prop-types';
import React from 'react';

import { FormControl, MenuItem, InputLabel, Select as MuiSelect } from '@material-ui/core';
import isMobile from 'is-mobile';

function SelectDesktop({ id, label, onChange, value, values, ...rest }) {
	const labelId = `${id}-label`;
	return (
		<>
			<InputLabel id={labelId}>{label}</InputLabel>
			<MuiSelect
				id={id}
				label={label}
				labelId={labelId}
				value={value}
				onChange={onChange}
				{...rest}
			>
				{values.map(x => <MenuItem key={x} value={x}>{x}</MenuItem>)}
			</MuiSelect>
		</>
	);
}

function SelectMobile({ id, label, onChange, value, values, ...rest }) {
	return (
		<>
			<InputLabel htmlFor={id}>{label}</InputLabel>
			<MuiSelect
				native
				id={id}
				label={label}
				value={value}
				onChange={onChange}
				{...rest}
			>
				{values.map(x => <option key={x} value={x}>{x}</option>)}
			</MuiSelect>
		</>
	);
}

function Select({ fullWidth, ...rest }) {
	return (
		<FormControl fullWidth={fullWidth}>
			{
				isMobile()
					? <SelectMobile {...rest} />
					: <SelectDesktop {...rest} />
			}
		</FormControl>
	);
}

const stringOrNumber = PropTypes.oneOfType([PropTypes.string, PropTypes.number]);

Select.propTypes = {
	fullWidth: PropTypes.bool,
	id: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	value: stringOrNumber,
	values: PropTypes.arrayOf(stringOrNumber).isRequired,
};

Select.defaultProps = {
	fullWidth: false,
	value: '',
};

SelectDesktop.propTypes = {
	...Select.propTypes,
	value: stringOrNumber.isRequired,
};
SelectMobile.propTypes = SelectDesktop.propTypes;

export default Select;
export {
	SelectDesktop,
	SelectMobile,
};
