import PropTypes from 'prop-types';
import React from 'react';

import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useSchema } from 'src/SchemaProvider';

const useStyles = makeStyles(theme => ({
	root: {
		backdropFilter: theme.mixins.glass.backdropFilter,
		backgroundColor: ({ getGpaColor }) => theme.mixins.glass.getBackgroundColor(getGpaColor(theme), theme.palette.type),
		border: '1px solid',
		borderColor: ({ getGpaColor }) => `rgb(${getGpaColor(theme).join(' ')})`,
		borderRadius: theme.shape.borderRadius,
		display: 'inline-block',
		padding: theme.spacing(0.75, 1),
		transition: theme.transitions.create(['background-color', 'border'], {
			duration: theme.transitions.duration.shortest,
		}),
	},
}));

function descendingWithStableNulls(a, b) {
	if (a == null || b == null || a === b) {
		return 0;
	}

	if (a > b) {
		return -1;
	}

	return 1;
}

function getGpaColor({ gpa, palette, thresholds }) {
	return palette[Math.max(
		[...thresholds, gpa].sort(descendingWithStableNulls).indexOf(gpa),
		thresholds.findIndex(x => x != null),
	)];
}

function Gpa({ className, gpa, typographyVariant, ...rest }) {
	const schema = useSchema();

	const formatted = (Number.isNaN(gpa) || gpa == null) ? '—' : gpa.toFixed(schema.gpa.decimalPoints);
	const getGpaColorPartial = theme => getGpaColor({ gpa, palette: theme.gpaPalette, thresholds: schema.gpa.thresholds });

	const styles = useStyles({
		getGpaColor: getGpaColorPartial,
	});

	return (
		<Typography variant={typographyVariant} component="span" className={clsx(styles.root, className)} {...rest}>
			{formatted}
		</Typography>
	);
}

Gpa.propTypes = {
	className: PropTypes.string,
	gpa: PropTypes.number.isRequired,
	typographyVariant: PropTypes.string,
};

Gpa.defaultProps = {
	className: '',
	typographyVariant: 'body2',
};

export default Gpa;
