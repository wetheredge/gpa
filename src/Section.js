import PropTypes from 'prop-types';
import React from 'react';

import { Box, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
	root: {
		[theme.breakpoints.only('xs')]: {
			borderLeft: 'none',
			borderRadius: 0,
			borderRight: 'none',
		},
	},
}));

function Section({ children, className, ...props }) {
	const styles = useStyles();

	return (
		<Box p={2} component={Paper} className={clsx(className, styles.root)} {...props}>
			{children}
		</Box>
	);
}

Section.propTypes = {
	children: PropTypes.node.isRequired,
	className: PropTypes.string,
};

Section.defaultProps = {
	className: '',
};

export default Section;
