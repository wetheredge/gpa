import { byKey } from 'src/utils';

function calculateGpa(term) {
	const points = term.reduce((acc, { points }) => acc + points, 0);
	const credits = term.reduce((acc, { credits }) => acc + credits, 0);

	const gpa = points / credits;

	return {
		credits,
		gpa,
		points,
	};
}

function getFieldValue(schema, data, key) {
	const field = schema.grade.find(byKey(key));

	if (field == null) {
		return;
	}

	const { mapping, type } = field;
	const value = data[key];
	return type === 'enum'
		? mapping[value]
		: value;
}

function getClassNormalizer(schema) {
	const keys = new Set(schema.grade.map(({ key }) => key));
	const hasCredits = schema.grade.some(byKey('credits'));

	const getClassPoints = data => {
		const credits = getFieldValue(schema, data, 'credits');
		let grade = getFieldValue(schema, data, 'grade');

		if (hasCredits) {
			grade *= credits;
		}

		for (const field of schema.grade) {
			if (field.purpose === 'multiplier') {
				let condition = true;

				if (field.condition != null) {
					let [operand1, comparison, operand2] = field.condition.split(' ');

					operand1 = keys.has(operand1) ? data[operand1] : Number.parseFloat(operand1);
					operand2 = keys.has(operand2) ? data[operand2] : Number.parseFloat(operand2);

					switch (comparison) {
						case '>':
							condition = operand1 > operand2;
							break;
						case '<':
							condition = operand1 < operand2;
							break;
						case '>=':
							condition = operand1 >= operand2;
							break;
						case '<=':
							condition = operand1 <= operand2;
							break;
						case '==':
						case '===':
							condition = operand1 === operand2;
							break;
						case '!=':
						case '!==':
							condition = operand1 !== operand2;
							break;
						default:
							condition = false;
					}
				}

				if (condition) {
					grade *= getFieldValue(schema, data, field.key);
				}
			}
		}

		return grade;
	};

	return class_ => {
		return {
			credits: hasCredits ? getFieldValue(schema, class_, 'credits') : 1,
			points: getClassPoints(class_),
		};
	};
}

function getClassValidator(schema) {
	const hasCredits = schema.grade.some(byKey('credits'));
	const gradeField = schema.grade.find(byKey('grade'));

	return class_ => !(
		class_.grade == null
		|| class_.grade === ''
		|| (gradeField.type === 'enum' && !(class_.grade in gradeField.mapping))
		|| (hasCredits && (class_.credits === '' || class_.credits == null))
	);
}

function getGpas(schema, terms) {
	const normalizeClass = getClassNormalizer(schema);
	const validateClass = getClassValidator(schema);

	const termGpas = Object.fromEntries(
		terms
			.map(({ classes, id }) => {
				const cleanedClasses = classes
					.filter(validateClass(?))
					.map(normalizeClass(?));

				if (cleanedClasses.length > 0) {
					return [id, calculateGpa(cleanedClasses)];
				}

				return null;
			})
			.filter(x => x != null),
	);

	const { gpa } = calculateGpa(Object.values(termGpas));
	const termsWithoutCredits = Object.fromEntries(
		Object.entries(termGpas)
			.map(([key, { gpa }]) => [key, gpa]),
	);

	return {
		final: gpa,
		terms: termsWithoutCredits,
	};
}

export default getGpas;
