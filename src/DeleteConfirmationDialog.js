import PropTypes from 'prop-types';
import React from 'react';

import { Button, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
	root: {
		maxWidth: '250px',
	},
});

function DeleteConfirmationDialog({ dispatch, open }) {
	const styles = useStyles();

	const handleTermDeleteCancel = () => dispatch({ type: 'terms.delete.cancel' });
	const handleTermDeleteConfirm = () => dispatch({ type: 'terms.delete.confirm' });

	return (
		<Dialog
			fullWidth
			open={open}
			aria-labelledby="term-delete-dialog"
			PaperProps={{ className: styles.root }}
			onClose={handleTermDeleteCancel}
		>
			<DialogContent id="term-delete-dialog">Delete term?</DialogContent>
			<DialogActions>
				<Button color="primary" onClick={handleTermDeleteCancel}>
					Cancel
				</Button>
				{/* eslint-disable-next-line jsx-a11y/no-autofocus -- see MDN for dialog role, section Focus Management */}
				<Button autoFocus color="primary" onClick={handleTermDeleteConfirm}>
					Delete
				</Button>
			</DialogActions>
		</Dialog>
	);
}

DeleteConfirmationDialog.propTypes = {
	dispatch: PropTypes.func.isRequired,
	open: PropTypes.bool.isRequired,
};

export default DeleteConfirmationDialog;
