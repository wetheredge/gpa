import PropTypes from 'prop-types';
import React from 'react';

import { Menu, MenuItem } from '@material-ui/core';
import Link from 'next/link';

function MobileNavMenu({ items, onClose: handleClose, ...props }) {
	return (
		<Menu onClose={handleClose} {...props}>
			{items.map(({ name, selected, url }) => (
				<Link
					key={url}
					passHref
					href={url}
				>
					<MenuItem
						selected={selected}
						component="a"
						onClick={handleClose}
					>
						{name}
					</MenuItem>
				</Link>
			))}
		</Menu>
	);
}

MobileNavMenu.propTypes = {
	items: PropTypes.arrayOf(PropTypes.shape({
		name: PropTypes.string.isRequired,
		selected: PropTypes.bool.isRequired,
		url: PropTypes.string.isRequired,
	}).isRequired).isRequired,
	onClose: PropTypes.func.isRequired,
};

export default MobileNavMenu;
