import fs from 'fs/promises';

async function getSchema(school) {
	const schema = await import(`schemas/${school}`).then(mod => mod.default);
	schema.id = school;
	return schema;
}

async function getSchools() {
	const files = await fs.readdir('./schemas/');
	const schools = files.map(file => file.split('.')[0]);
	return Array.from(new Set(schools));
}

async function getAllSchemas() {
	const schools = await getSchools();
	return Promise.all(schools.map(getSchema(?)));
}

export {
	getAllSchemas,
	getSchema,
	getSchools,
};
