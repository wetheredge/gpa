import PropTypes from 'prop-types';
import React from 'react';

import { Grid, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { AddTwoTone, DeleteTwoTone } from '@material-ui/icons';

import Grade from 'src/Grade';
import { useSchema } from 'src/SchemaProvider';

const useStyles = makeStyles({
	addButtonContainer: {
		textAlign: 'center',
	},
	pushRight: {
		marginLeft: 'auto',
	},
});

function GradeList({ classData, dispatch, termId }) {
	const schema = useSchema();
	const styles = useStyles({ thresholds: schema.gpaThresholds });

	const handleAddClass = () => dispatch({ termId, type: 'terms.classes.new' });
	const handleDeleteClass = classId => () => dispatch({ classId, termId, type: 'terms.classes.delete' });

	return (
		<>
			{classData.map(({ id: classId, ...rest }) => (
				<Grid key={classId} item container spacing={1} alignItems="center">
					<Grade
						classId={classId}
						termId={termId}
						value={rest}
						dispatch={dispatch}
					/>
					<Grid item>
						<IconButton aria-label="delete" onClick={handleDeleteClass(classId)}>
							<DeleteTwoTone />
						</IconButton>
					</Grid>
				</Grid>
			))}
			<Grid item xs={12} className={styles.addButtonContainer}>
				<IconButton aria-label="add" onClick={handleAddClass}>
					<AddTwoTone />
				</IconButton>
			</Grid>
		</>
	);
}

GradeList.propTypes = {
	classData: PropTypes.arrayOf(PropTypes.object).isRequired,
	dispatch: PropTypes.func.isRequired,
	termId: PropTypes.string.isRequired,
};

export default GradeList;
