import { createContext, useContext } from 'react';

const Schema = createContext();
const useSchema = () => useContext(Schema);
const SchemaProvider = Schema.Provider;

export default SchemaProvider;
export {
	useSchema,
};
