import PropTypes from 'prop-types';
import React from 'react';

import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import Section from 'src/Section';

const useStyles = makeStyles(theme => ({
	root: {
		'& > :first-child': {
			borderBottomLeftRadius: 0,
			borderBottomRightRadius: 0,
			padding: theme.spacing(1, 2),
			top: `-${theme.shape.borderRadius + theme.spacing(0.5)}px`,
			zIndex: theme.zIndex.appBar,
		},
		'& > :last-child': {
			borderTop: 'none',
			borderTopLeftRadius: 0,
			borderTopRightRadius: 0,
		},
	},
	sticky: {
		'& > :first-child': {
			position: 'sticky',
		},
	},
}));

function SplitSectionHeader({ children, justifyContent }) {
	return (
		<Section>
			<Grid container direction="row" alignItems="center" justify={justifyContent}>
				{children}
			</Grid>
		</Section>
	);
}

SplitSectionHeader.propTypes = {
	children: PropTypes.node.isRequired,
	justifyContent: PropTypes.oneOf(['flex-start', 'center', 'flex-end', 'space-between', 'space-around', 'space-evenly']),
};

SplitSectionHeader.defaultProps = {
	justifyContent: 'space-between',
};

function SplitSectionBody({ children, spacing }) {
	return (
		<Section>
			<Grid container spacing={spacing}>
				{children}
			</Grid>
		</Section>
	);
}

SplitSectionBody.propTypes = {
	children: PropTypes.node.isRequired,
	spacing: PropTypes.number,
};

SplitSectionBody.defaultProps = {
	spacing: 0,
};

function SplitSection({ children, component: Component, noSticky }) {
	const styles = useStyles();

	return (
		<Component className={clsx(
			styles.root,
			{
				[styles.sticky]: !noSticky,
			},
		)}
		>{children}
		</Component>
	);
}

SplitSection.propTypes = {
	children: PropTypes.node.isRequired,
	component: PropTypes.elementType,
	noSticky: PropTypes.bool,
};

SplitSection.defaultProps = {
	component: 'div',
	noSticky: false,
};

export default SplitSection;
export {
	SplitSectionBody,
	SplitSectionHeader,
};
