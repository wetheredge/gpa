import { typeOf } from './typeOf';

test.each([
	[[], 'array'],
	[new Date(), 'date'],
	[() => {}, 'function'],
	[new Map(), 'map'],
	[null, 'null'],
	[{}, 'object'],
	[/./, 'regExp'],
	[new Set(), 'set'],
	['', 'string'],
	[Symbol(''), 'symbol'],
	[undefined, 'undefined'],
])('typeOf(%p) => %s', (value, type) => expect(typeOf(value)).toBe(type));
