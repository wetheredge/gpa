import { normalizeSchema } from './state';

test('adds mapping and changes values to strings for enum grade fields with only number values and no mapping', () => {
	const schema = normalizeSchema({
		defaultTermName: 'Test',
		grade: [{
			key: 'enum',
			label: 'Enum',
			type: 'enum',
			values: [0, 1],
		}],
		id: 'test',
	});

	expect(schema.grade[0].mapping).toStrictEqual({ 0: 0, 1: 1 });
	expect(schema.grade[0].values).toStrictEqual(['0', '1']);
});
