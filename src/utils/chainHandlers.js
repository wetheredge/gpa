function chainHandlers(...handlers) {
	return (...args) => {
		for (const handler of handlers) {
			handler(...args);
		}
	};
}

export {
	chainHandlers,
};
