const is = test => typeof test === 'function' ? test : x => x === test;
const not = test => x => !is(test)(x);

const byField = (field, test) => toTest => is(test)(toTest[field]);
const byId = byField('id', ?);
const byKey = byField('key', ?);

export {
	byField,
	byId,
	byKey,
	is,
	not,
};
