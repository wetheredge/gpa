function typeOf(x) {
	const type = typeof x;
	const format = ([first, ...rest]) => first.toLowerCase() + rest.join('');

	if (type !== 'object') {
		return format(type);
	}

	if (x === null) {
		return 'null';
	}

	if (Array.isArray(x)) {
		return 'array';
	}

	return format(toString.call(x).slice(8, -1));
}

export {
	typeOf,
};
