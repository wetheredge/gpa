import getId from 'src/getId';

import { byId } from './filters';

function getInitialTerms(currentSchool, allSchemas) {
	const currentSchema = allSchemas.find(byId(currentSchool));
	const id = getId();

	const terms = Object.fromEntries(allSchemas.map(schema => [schema.id, [newTerm(schema)]]));
	terms[currentSchema.id] = [newTerm(currentSchema, {}, { id })];

	return terms;
}

function newClass(schema, overrides = {}) {
	const defaults = Object.fromEntries(schema.grade.map(({ default: value, key }) => [key, value]));

	return {
		id: getId(),
		...defaults,
		...overrides,
	};
}

function newTerm(schema, overrides = {}, classOverrides = {}) {
	return {
		classes: [newClass(schema, classOverrides)],
		id: getId(),
		name: schema.defaultTermName,
		...overrides,
	};
}

function normalizeSchema({ grade, ...schema }) {
	return {
		...schema,
		grade: grade.map(original => {
			const { type } = original;

			const normalized = { ...original };
			normalized.default ??= '';
			normalized.width ??= 'wide';

			if (type === 'number') {
				normalized.min ??= 0;
				normalized.step ??= 1;
			} else if (type === 'enum' && original.mapping == null && original.values.some(x => typeof x !== 'string')) {
				normalized.mapping = Object.fromEntries(original.values.map(x => [x.toString(), x]));
				normalized.values = original.values.map(x => x.toString());
			}

			return normalized;
		}),
	};
}

function termsToState(schema, terms, { focusFirstClass = false } = {}) {
	let focus = null;
	if (focusFirstClass) {
		const firstTerm = terms[schema.id][0];
		const firstClass = firstTerm.classes[0];

		focus = `${schema.grade[0].key}-${firstClass.id}${firstTerm.id}`;
	}

	return {
		delete: null,
		editingTermName: null,
		focus,
		terms,
	};
}

export {
	getInitialTerms,
	newClass,
	newTerm,
	normalizeSchema,
	termsToState,
};
