import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';

import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@material-ui/core';

function RenameTermDialog({ dispatch, initialName, open }) {
	const [name, setName] = useState();

	useEffect(() => {
		if (open) {
			setName(initialName);
		}
	}, [open, initialName]);

	const handleChangeName = ({ target }) => setName(target.value);

	const handleCancel = () => dispatch({ type: 'terms.editName.cancel' });
	const handleConfirm = () => dispatch({ payload: name, type: 'terms.editName.confirm' });

	return (
		<Dialog
			fullWidth
			open={open}
			aria-labelledby="term-rename-dialog"
			maxWidth="xs"
			onClose={handleCancel}
		>
			<DialogTitle id="term-rename-dialog">Edit term name</DialogTitle>
			<DialogContent>
				<TextField
					fullWidth
					id="term-rename-input"
					label="New name"
					value={name}
					InputLabelProps={{ shrink: true }}
					onChange={handleChangeName}
				/>
			</DialogContent>
			<DialogActions>
				<Button color="primary" onClick={handleCancel}>
					Cancel
				</Button>
				{/* eslint-disable-next-line jsx-a11y/no-autofocus -- see MDN for dialog role, section Focus Management */}
				<Button autoFocus color="primary" onClick={handleConfirm}>
					OK
				</Button>
			</DialogActions>
		</Dialog>
	);
}

RenameTermDialog.propTypes = {
	dispatch: PropTypes.func.isRequired,
	initialName: PropTypes.string.isRequired,
	open: PropTypes.bool.isRequired,
};

export default RenameTermDialog;
