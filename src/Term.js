import PropTypes from 'prop-types';
import React from 'react';

import { Grid, IconButton, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { MoreVertTwoTone } from '@material-ui/icons';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';

import Gpa from 'src/Gpa';
import GradeList from 'src/GradeList';
import { useSchema } from 'src/SchemaProvider';
import SplitSection, { SplitSectionBody, SplitSectionHeader } from 'src/SplitSection';
import TermMenu from 'src/TermMenu';

const useStyles = makeStyles({
	addButtonContainer: {
		textAlign: 'center',
	},
	pushRight: {
		marginLeft: 'auto',
	},
});

function Term({ classData, dispatch, gpa, id, name }) {
	const schema = useSchema();
	const styles = useStyles({ thresholds: schema.gpaThresholds });

	return (
		<SplitSection component="section">
			<SplitSectionHeader>
				<Grid item>
					<Typography variant="subtitle1">{name}</Typography>
				</Grid>
				<Grid item className={styles.pushRight}>
					<Gpa gpa={gpa} />
				</Grid>
				<Grid item>
					<PopupState variant="popover" popupId={`actions.${id}`}>
						{popupState => (
							<>
								<IconButton id={`menu.${id}`} edge="end" aria-label="show term actions" {...bindTrigger(popupState)}>
									<MoreVertTwoTone />
								</IconButton>
								<TermMenu
									termId={id}
									dispatch={dispatch}
									onClose={popupState.close}
									{...bindMenu(popupState)}
								/>
							</>
						)}
					</PopupState>
				</Grid>
			</SplitSectionHeader>
			<SplitSectionBody spacing={2}>
				<GradeList classData={classData} termId={id} dispatch={dispatch} />
			</SplitSectionBody>
		</SplitSection>
	);
}

Term.propTypes = {
	classData: PropTypes.arrayOf(PropTypes.object).isRequired,
	dispatch: PropTypes.func.isRequired,
	gpa: PropTypes.number.isRequired,
	id: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
};

export default Term;
