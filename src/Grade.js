import PropTypes from 'prop-types';
import React from 'react';

import { Box, Grid, TextField } from '@material-ui/core';

import { useSchema } from 'src/SchemaProvider';
import Select from 'src/Select';

function Field({ classId, dispatch, schema, termId, value }) {
	const onChange = event => {
		const raw = event.target.value;

		const payload = schema.type === 'number'
			? do {
				const parsed = Number.parseInt(raw, 10);
				// eslint-disable-next-line no-unused-expressions
				Number.isNaN(parsed) ? 0 : parsed;
			} : raw;

		dispatch({
			classId,
			key: schema.key,
			payload,
			termId,
			type: 'terms.classes.update',
		});
	};

	const fieldProps = {
		fullWidth: true,
		id: `${schema.key}-${classId}${termId}`,
		label: schema.label,
		onChange,
		value,
	};

	if (schema.type === 'number') {
		fieldProps.inputProps ??= {};

		if (schema.max != null) {
			fieldProps.inputProps.max = schema.max;
		}

		if (schema.min != null) {
			fieldProps.inputProps.min = schema.min;
		}

		if (schema.step != null) {
			fieldProps.inputProps.step = schema.step;
		}
	}

	return schema.type === 'enum'
		? <Select {...fieldProps} values={schema.values} />
		: <TextField {...fieldProps} type={schema.type} />;
}

Field.propTypes = {
	classId: PropTypes.string.isRequired,
	dispatch: PropTypes.func.isRequired,
	schema: PropTypes.object.isRequired,
	termId: PropTypes.string.isRequired,
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

function Grade({ classId, dispatch, termId, value }) {
	const schema = useSchema();
	const baseWidth = 72;

	return (
		<>
			{schema.grade.map(thisSchema => (
				<Grid
					key={thisSchema.key}
					item
					component={Box}
					flexGrow={1}
					flexShrink={0}
					flexBasis={baseWidth}
					minWidth={{ medium: baseWidth * 1.25, wide: baseWidth * 1.5 }[thisSchema.width] ?? baseWidth}
				>
					<Field
						schema={thisSchema}
						classId={classId}
						dispatch={dispatch}
						termId={termId}
						value={value[thisSchema.key]}
					/>
				</Grid>
			))}
		</>
	);
}

Grade.propTypes = {
	classId: PropTypes.string.isRequired,
	dispatch: PropTypes.func.isRequired,
	termId: PropTypes.string.isRequired,
	value: PropTypes.object.isRequired,
};

export default Grade;
