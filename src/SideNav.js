import PropTypes from 'prop-types';
import React from 'react';

import { ButtonBase, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Link from 'next/link';

import Section from 'src/Section';

const useStyles = makeStyles(theme => ({
	item: {
		'& a': {
			color: 'inherit',
			textDecoration: 'none',
		},

		'&:hover': {
			backgroundColor: theme.palette.action.hover,
		},

		borderColor: 'transparent',
		borderRadius: theme.shape.borderRadius,
		borderStyle: 'solid',
		borderWidth: '1px',
		font: 'inherit',
		justifyContent: 'flex-start',
		padding: theme.spacing(1),
		transition: theme.transitions.create(['background-color', 'border-color'], {
			duration: theme.transitions.duration.shortest,
		}),
	},
	itemActive: {
		'&:hover': {
			backgroundColor: theme.mixins.glass.getBackgroundColor(theme.palette.primary.main, theme.palette.type),
		},

		backdropFilter: theme.mixins.glass.backdropFilter,
		backgroundColor: theme.mixins.glass.getBackgroundColor(theme.palette.primary.main, theme.palette.type),
		borderColor: theme.palette.primary.main,
	},
	itemDisabled: {
		opacity: '50%',
	},
	section: {
		'& > * + *': {
			marginTop: theme.spacing(1),
		},

		'&:not(:first-child)': {
			borderTop: 'none',
			borderTopLeftRadius: 0,
			borderTopRightRadius: 0,
		},
		'&:not(:last-child)': {
			borderBottomLeftRadius: 0,
			borderBottomRightRadius: 0,
		},
	},
}));

function SideNav({ children, ...props }) {
	return (
		<Grid container component="nav" direction="column" xs={2} {...props}>
			{children}
		</Grid>
	);
}

SideNav.propTypes = {
	children: PropTypes.node.isRequired,
};

function SideNavItem({ disabled, isActive, text, url }) {
	const styles = useStyles();

	return (
		<Link passHref href={url}>
			<Grid
				item
				disabled={disabled}
				component={ButtonBase}
				className={clsx(
					styles.item,
					{
						[styles.itemActive]: isActive,
						[styles.itemDisabled]: disabled,
					},
				)}
			>
				{text}
			</Grid>
		</Link>
	);
}

SideNavItem.propTypes = {
	disabled: PropTypes.bool,
	isActive: PropTypes.bool.isRequired,
	text: PropTypes.string.isRequired,
	url: PropTypes.string.isRequired,
};

SideNavItem.defaultProps = {
	disabled: false,
};

function SideNavSection({ children }) {
	const styles = useStyles();

	return (
		<Grid item container component={Section} direction="column" className={styles.section}>
			{children}
		</Grid>
	);
}

SideNavSection.propTypes = {
	children: PropTypes.node.isRequired,
};

export default SideNav;
export {
	SideNavItem,
	SideNavSection,
};
