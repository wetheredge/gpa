import React from 'react';

import Link from 'next/link';

import { getAllSchemas } from 'src/pageGenUtils';

function Index({ schools }) {
	return (
		<ul>
			{Object.entries(schools).map(([key, name]) => (
				<li key={key}>
					<Link href={`/${key}`}>{name}</Link>
				</li>
			))}
		</ul>
	);
}

async function getStaticProps() {
	const allSchemas = await getAllSchemas();
	const schools = Object.fromEntries(allSchemas.map(({ id, name }) => [id, name]));

	return {
		props: {
			schools,
		},
	};
}

export default Index;
export {
	getStaticProps,
};
