import crypto from 'crypto';
import React from 'react';

import { ServerStyleSheets } from '@material-ui/core/styles';
import csso from 'csso';
import NextDocument, { Head, Html, Main, NextScript } from 'next/document';

function cspHash(text) {
	const hash = crypto.createHash('sha256');
	hash.update(text);
	return `'sha256-${hash.digest('base64')}'`;
}

class Document extends NextDocument {
	render() {
		const hash = cspHash(NextScript.getInlineScriptSource(this.props));
		const scriptSrc = process.env.NODE_ENV === 'production'
			? `'self' ${hash}`
			: `'self' 'unsafe-eval' ${hash}`;

		const csp = [
			"base-uri 'none'",
			"default-src 'self'",
			"connect-src 'self' https://vitals.vercel-insights.com/",
			"img-src 'self'",
			"manifest-src 'self'",
			"prefetch-src 'self'",
			`script-src ${scriptSrc}`,
			"style-src 'self' 'unsafe-inline'",
			"worker-src 'self'",
		].join(';');

		return (
			<Html lang="en">
				<Head>
					<meta http-equiv="Content-Security-Policy" content={csp} />
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

// `getInitialProps` belongs to `_document` (instead of `_app`),
// it's compatible with server-side generation (SSG).
Document.getInitialProps = async ctx => {
	// Render app and page and get the context of the page with collected side effects.
	const sheets = new ServerStyleSheets();
	const originalRenderPage = ctx.renderPage;

	ctx.renderPage = () =>
		originalRenderPage({
			enhanceApp: App => props => sheets.collect(<App {...props} />),
		});

	const initialProps = await NextDocument.getInitialProps(ctx);

	let css = sheets.toString();
	if (css && process.env.NODE_ENV === 'production') {
		({ css } = csso.minify(css));
	}

	return {
		...initialProps,
		styles: [
			...React.Children.toArray(initialProps.styles),
			<style
				key="jss-server-side"
				id="jss-server-side"
				// eslint-disable-next-line react/no-danger
				dangerouslySetInnerHTML={{ __html: css }}
			/>,
		],
	};
};

export default Document;
