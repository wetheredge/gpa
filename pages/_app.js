import PropTypes from 'prop-types';
import React, { useEffect } from 'react';

import { CssBaseline } from '@material-ui/core';
import Head from 'next/head';

import ThemeProvider from 'src/ThemeProvider';

function App({ Component, pageProps }) {
	useEffect(() => {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector('#jss-server-side');
		if (jssStyles) {
			jssStyles.remove();
		}
	}, []);

	return (
		<>
			<Head>
				<meta charSet="utf-8" />
				<meta name="viewport" content="width=device-width,initial-scale=1,viewport-fit=cover" />
				<title>GPA</title>

				<link rel="manifest" href="/manifest.webmanifest" />
				<meta name="theme-color" content="#bf84fb" />

				<link rel="icon" href="/favicon.svg" />
				<link rel="mask-icon" href="/mask-icon.svg" color="#bf84fb" />
				<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
			</Head>
			<ThemeProvider>
				<CssBaseline />
				<Component {...pageProps} />
			</ThemeProvider>
		</>
	);
}

App.propTypes = {
	Component: PropTypes.elementType.isRequired,
	pageProps: PropTypes.object.isRequired,
};

export default App;
