import React, { useEffect, useReducer } from 'react';

import { Container, Grid, Hidden, IconButton, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { AddTwoTone, MenuTwoTone, SettingsTwoTone } from '@material-ui/icons';
import clsx from 'clsx';
import { debounce } from 'lodash';
import PopupState, { bindMenu, bindTrigger } from 'material-ui-popup-state';
import Head from 'next/head';
import Link from 'next/link';

import DeleteConfirmationDialog from 'src/DeleteConfirmationDialog';
import getGpas from 'src/getGpas';
import Gpa from 'src/Gpa';
import MobileNavMenu from 'src/MobileNavMenu';
import { getAllSchemas, getSchools } from 'src/pageGenUtils';
import getReducer from 'src/reducer';
import RenameTermDialog from 'src/RenameTermDialog';
import SchemaProvider from 'src/SchemaProvider';
import Section from 'src/Section';
import SideNav, { SideNavItem, SideNavSection } from 'src/SideNav';
import Term from 'src/Term';
import { byId, getInitialTerms, normalizeSchema, termsToState } from 'src/utils';

const useStyles = makeStyles(theme => ({
	'@global': {
		'#__next': {
			'& > * + *': {
				marginTop: theme.spacing(3),
			},

			alignItems: 'center',
			display: 'flex',
			flexDirection: 'column',
			height: '100%',
		},
		'html, body': {
			height: '100%',
		},
	},
	addButtonContainer: {
		textAlign: 'center',
	},
	bottomBar: {
		borderBottomLeftRadius: 0,
		borderBottomRightRadius: 0,
		borderBottomWidth: 0,
		boxShadow: theme.palette.type === 'dark' ? '0px -1px 4px 0px rgb(0 0 0 / .75)' : '',
		paddingBottom: `max(${theme.spacing(1)}px, ${theme.mixins.safeAreas.bottom()})`,
		paddingTop: theme.spacing(1),
		textAlign: 'center',
	},
	bottomBarSchool: {
		textAlign: 'left',
	},
	bottomBarSettings: {
		textAlign: 'right',
	},
	bottomBarWrapper: {
		bottom: 0,
		position: 'fixed',
		zIndex: theme.zIndex.appBar,
	},
	center: {
		'& > * + *': {
			marginTop: theme.spacing(2),
		},

		display: 'flex',
		flexDirection: 'column',

		[theme.breakpoints.up('sm')]: {
			marginTop: theme.spacing(2),
		},

		[theme.breakpoints.only('xs')]: {
			paddingLeft: 0,
			paddingRight: 0,
		},
	},
	container: {
		flex: '1 0 auto',
	},
	paddingForMobileBar: {
		// Padding between items (from #__next > * + *) + measured height
		marginBottom: theme.spacing(3) + 65,
	},
	sideBox: {
		position: 'sticky',
		top: theme.spacing(2),
	},
	sideBoxGpa: {
		borderWidth: '2px',
		display: 'block',
		margin: 'auto',
		padding: theme.spacing(2),
		textAlign: 'center',
		width: '50%',
	},
}));

function School({ schema, schools, ssrTerms }) {
	const [state, dispatch] = useReducer(getReducer(schema), ssrTerms, termsToState(schema, ?, { focusFirstClass: true }));
	const terms = state.terms[schema.id];

	const handleAddTerm = () => dispatch({ type: 'terms.new' });

	useEffect(() => {
		if (typeof window === 'undefined') {
			return;
		}

		const termFilter = key => key.startsWith('term ');
		const handleReplaceTerms = (keys = Object.keys(localStorage)) => {
			dispatch({
				payload: Object.fromEntries(
					keys
						.filter(termFilter(?))
						.map(key => [
							key.split(' ')[1],
							JSON.parse(localStorage.getItem(key)),
						]),
				),
				type: 'terms.replace',
			});
		};

		handleReplaceTerms();

		const handleStorage = debounce(event => {
			if (document.visibilityState === 'visible' && termFilter(event.key)) {
				handleReplaceTerms([event.key]);
			}
		}, 1000);

		const handleVisibility = () => {
			if (document.visibilityState === 'visible') {
				handleReplaceTerms();
			}
		};

		window.addEventListener('storage', handleStorage, { passive: true });
		window.addEventListener('visibilitychange', handleVisibility);

		return () => {
			window.removeEventListener('storage', handleStorage);
			window.removeEventListener('visibilitychange', handleVisibility);
		};
	}, [dispatch]);

	useEffect(() => {
		if (!state.focus) {
			return;
		}

		const elementToFocus = document.querySelector(`#${state.focus}`);

		if (elementToFocus == null) {
			console.error(`Tried to focus element that does not exist: ${state.focus}`);
		} else {
			elementToFocus.focus();
			dispatch({ type: 'focus.clear' });
		}
	});

	const gpas = getGpas(schema, terms);
	const styles = useStyles({ gpa: gpas.final, thresholds: schema.gpa.thresholds });

	const sortedSchoolEntries = Object.entries(schools).sort(([_aKey, aName], [_bKey, bName]) => aName < bName ? -1 : (aName > bName ? 1 : 0));

	return (
		<>
			<Head>
				<title>{schema.name} GPA</title>
			</Head>

			<SchemaProvider value={schema}>
				<Grid container alignItems="flex-start" justify="center" className={styles.container}>
					<Hidden smDown>
						<Grid item component={SideNav} className={styles.sideBox}>
							<SideNavSection>
								{sortedSchoolEntries.map(([key, name]) => (
									<SideNavItem
										key={key}
										isActive={name === schema.name}
										text={name}
										url={`/${key}`}
									/>
								))}
							</SideNavSection>
							<SideNavSection>
								<SideNavItem disabled isActive={false} text="Settings" url="/settings" />
							</SideNavSection>
						</Grid>
					</Hidden>
					<Grid item className={styles.paddingForMobileBar}>
						<Container
							maxWidth="sm"
							component="main"
							className={styles.center}
						>
							{terms.map(({ classes, id, name }) => {
								return (
									<Term
										key={id}
										id={id}
										name={name}
										classData={classes}
										gpa={gpas.terms[id] ?? Number.NaN}
										dispatch={dispatch}
									/>
								);
							})}

							<div className={styles.addButtonContainer}>
								<IconButton aria-label="add term" onClick={handleAddTerm}>
									<AddTwoTone />
								</IconButton>
							</div>
						</Container>
					</Grid>
					<Hidden smDown>
						<Grid item xs={2} className={styles.sideBox}>
							<Gpa gpa={gpas.final} typographyVariant="h6" className={styles.sideBoxGpa} />
						</Grid>
					</Hidden>
				</Grid>

				<Hidden mdUp>
					<Container maxWidth="sm" className={clsx(styles.center, styles.bottomBarWrapper)}>
						<Section className={styles.bottomBar} spacing={1}>
							<Grid container alignItems="center">
								<Grid item>
									<PopupState variant="popover" popupId="school-popup">
										{popupState => (
											<>
												<IconButton edge="start" aria-label="open school chooser" {...bindTrigger(popupState)}>
													<MenuTwoTone />
												</IconButton>
												<MobileNavMenu
													items={sortedSchoolEntries.map(([key, name]) => ({
														name,
														selected: name === schema.name,
														url: `/${key}`,
													}))}
													handleClose={popupState.close}
													{...bindMenu(popupState)}
												/>
											</>
										)}
									</PopupState>
								</Grid>
								<Grid item xs className={styles.bottomBarSchool}>
									<Typography variant="subtitle1">
										{schema.name}
									</Typography>
								</Grid>
								<Grid item xs={4}>
									<Gpa gpa={gpas.final} />
								</Grid>
								<Grid item xs={4} className={styles.bottomBarSettings}>
									<Link passHref href="/settings">
										<IconButton disabled edge="end" aria-label="go to settings">
											<SettingsTwoTone />
										</IconButton>
									</Link>
								</Grid>
							</Grid>
						</Section>
					</Container>
				</Hidden>

				<DeleteConfirmationDialog dispatch={dispatch} open={state.delete != null} />
				<RenameTermDialog
					dispatch={dispatch}
					initialName={terms.find(byId(state.editingTermName))?.name}
					open={state.editingTermName != null}
				/>
			</SchemaProvider>
		</>
	);
}

async function getStaticPaths() {
	const schools = await getSchools();

	return {
		fallback: false,
		paths: schools.map(school => ({
			params: { school },
		})),
	};
}

async function getStaticProps({ params: { school } }) {
	const allSchemas = (await getAllSchemas()).map(normalizeSchema(?));
	const ssrTerms = getInitialTerms(school, allSchemas);

	const schools = Object.fromEntries(allSchemas.map(({ id, name }) => [id, name]));

	return {
		props: {
			schema: allSchemas.find(byId(school)),
			schools,
			ssrTerms,
		},
	};
}

export default School;
export {
	getStaticPaths,
	getStaticProps,
};
