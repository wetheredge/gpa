module.exports = {
	envs: [
		'browser',
		'commonjs',
	],
	extends: [
		'xo-react',
		'plugin:jsx-a11y/strict',
		'plugin:react-hooks/recommended',
	],
	ignores: 'public/sw.js',
	overrides: [
		{
			files: ['pages/api/**/*.js', '*.config.js'],
			rules: { 'import/no-commonjs': 'off' },
		},
		{
			files: [
				'src/*[uU]tils.js',
				'src/*[uU]tils/**.js',
			],
			rules: {
				'import/no-default-export': 'error',
				'import/prefer-default-export': 'off',
			},
		},
		{
			files: [
				'src/*/index.js',
			],
			rules: {
				'import/no-default-export': 'off',
				'import/prefer-default-export': 'error',
			},
		},
		{
			files: [
				'pages/**/[^_]*.js',
				'pages/_app.js',
				'pages/_document.js',
			],
			rules: {
				'react/prop-types': 'off',
			},
		},
		{
			envs: ['browser', 'jest'],
			files: ['jest.setup.js', '**.test.js'],
			rules: {
				'max-nested-callbacks': 'off',
			},
		},
		{
			envs: ['serviceworker', 'browser'],
			files: 'public/sw.js',
		},
	],
	parser: '@babel/eslint-parser',
	plugins: [
		'sort-destructure-keys',
	],
	rules: {
		'capitalized-comments': [
			'error',
			'always',
			{ ignoreConsecutiveComments: true },
		],
		'comma-dangle': ['error', 'always-multiline'],
		eqeqeq: ['error', 'smart'],
		'import/exports-last': 'error',
		'import/extensions': ['error', 'never'],
		'import/group-exports': 'error',
		'import/no-anonymous-default-export': [
			'error',
			{
				allowArrowFunction: true,
				allowLiteral: true,
				allowObject: true,
			},
		],
		'import/no-commonjs': 'error',
		'import/no-default-export': 'off',
		'import/no-relative-parent-imports': 'error',
		'import/no-unassigned-import': 'error',
		'import/order': [
			'error',
			{
				alphabetize: {
					caseInsensitive: true,
					order: 'asc',
				},
				groups: [
					'builtin',
					'external',
					'internal',
					'parent',
					'sibling',
					'index',
				],
				'newlines-between': 'always',
			},
		],
		'import/prefer-default-export': 'error',
		'jsx-a11y/label-has-for': 'off',
		'no-eq-null': 'off',
		'no-multiple-empty-lines': [
			'error',
			{
				max: 1,
				maxBOF: 0,
				maxEOF: 0,
			},
		],
		'no-unused-vars': [
			'error',
			{
				args: 'all',
				argsIgnorePattern: '^_',
				caughtErrors: 'all',
				caughtErrorsIgnorePattern: '^_',
				ignoreRestSiblings: false,
				vars: 'all',
				varsIgnorePattern: '^_',
			},
		],
		'node/file-extension-in-import': ['error', 'never'],
		'object-curly-spacing': ['error', 'always'],
		'operator-linebreak': ['error', 'before'],
		quotes: [
			'error',
			'single',
			{ avoidEscape: true },
		],
		'react/boolean-prop-naming': 'off',
		'react/function-component-definition': [
			'error',
			{
				namedComponents: 'function-declaration',
				unnamedComponents: 'arrow-function',
			},
		],
		'react/jsx-tag-spacing': [
			'error',
			{ beforeSelfClosing: 'always' },
		],
		'react/no-unknown-property': [
			'error',
			{ ignore: ['http-equiv'] },
		],
		'sort-destructure-keys/sort-destructure-keys': [
			'error',
			{ caseSensitive: true },
		],
		'sort-keys': [
			'error',
			'asc',
			{
				caseSensitive: true,
				natural: true,
			},
		],
		'unicorn/filename-case': 'off',
		'unicorn/no-array-reduce': 'off',
		'unicorn/no-unsafe-regex': 'error',
		'unicorn/prefer-replace-all': 'error',
	},
	settings: {
		'import/core-modules': ['react', 'prop-types'],
		'import/internal-regex': '^(src|pages|schemas)/',
	},
};
