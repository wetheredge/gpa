module.exports = {
	moduleDirectories: ['node_modules', '<rootDir>'],
	reporters: ['default', 'jest-junit'],
	setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
};
