const bundleAnalyzer = require('@next/bundle-analyzer');
const withPlugins = require('next-compose-plugins');
const env = require('next-env');
const preact = require('next-plugin-preact');
const pwa = require('next-pwa');

module.exports = withPlugins([
	bundleAnalyzer({
		enabled: process.env.ANALYZE === 'true',
	}),
	[pwa, {
		pwa: {
			dest: 'public',
		},
	}],
	env,
	preact,
], {
	async headers() {
		const format = headers => Object.entries(headers).map(([key, value]) => ({
			key,
			value: Array.isArray(value)
				? value.join('; ')
				: value,
		}));

		return [
			{
				headers: format({
					'Referrer-Policy': 'no-referrer',
					'X-Content-Type-Options': 'nosniff',
					'X-Frame-Options': 'deny',
					'X-UA-Compatible': 'IE=edge',
					'X-XSS-Protection': [
						'1',
						'mode=block',
					],
				}),
				source: '/(.*)',
			},
			{
				headers: format({
					'Content-Security-Policy': [
						"default-src 'none'",
						"frame-ancestors 'none'",
					],
				}),
				source: '/api/(.*)',
			},
		];
	},
	poweredByHeader: false,
	reactStrictMode: true,
});
