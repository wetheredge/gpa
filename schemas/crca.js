function getDefaultTermName() {
	const now = new Date();
	const year = now.getFullYear();
	const month = now.getMonth();

	const format = first => `${first}-${(first + 1).toString().slice(-2)}`;

	if (month < 7) {
		return format(year - 1);
	}

	return format(year);
}

export default {
	defaultTermName: getDefaultTermName(),
	gpa: {
		decimalPoints: 3,
		thresholds: [100, 90, 80, 70, 60],
	},
	grade: [
		{
			key: 'name',
			label: 'Class name ',
			type: 'text',
		},
		{
			condition: 'grade >= 70',
			default: 'Regular',
			key: 'type',
			label: 'Type',
			mapping: {
				AP: 1.17,
				'Pre-AP': 1.15,
				Regular: 1,
				acc: 1.17,
			},
			purpose: 'multiplier',
			type: 'enum',
			values: ['Regular', 'Pre-AP', 'AP'],
			width: 'medium',
		},
		{
			default: 1,
			key: 'credits',
			label: 'Credits',
			type: 'enum',
			values: [0, 0.5, 1],
			width: 'narrow',
		},
		{
			key: 'grade',
			label: 'Grade',
			max: 100,
			type: 'number',
			width: 'narrow',
		},
	],
	name: 'CRCA',
};
