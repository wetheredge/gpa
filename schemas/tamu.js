function getDefaultTermName() {
	const now = new Date();
	const year = now.getFullYear();
	const month = now.getMonth();
	const day = now.getDay();

	if (month < 5) {
		return `Spring ${year}`;
	}

	if (month <= 7 && day < 17) {
		return `Summer ${year}`;
	}

	return `Fall ${year}`;
}

export default {
	defaultTermName: getDefaultTermName(),
	gpa: {
		decimalPoints: 3,
		thresholds: [null, 4, 3, 2, 1],
	},
	grade: [
		{
			key: 'name',
			label: 'Class name',
			type: 'text',
		},
		{
			default: 4,
			key: 'credits',
			label: 'Credits',
			type: 'number',
			width: 'medium',
		},
		{
			key: 'grade',
			label: 'Grade',
			mapping: { A: 4, B: 3, C: 2, D: 1, F: 0, U: 0 },
			type: 'enum',
			values: ['A', 'B', 'C', 'D', 'F', 'U'],
			width: 'medium',
		},
	],
	name: 'A&M',
};
