function getDefaultTermName() {
	const now = new Date();
	const year = now.getFullYear();
	const month = now.getMonth();
	const day = now.getDay();

	if (month < 5) {
		return `Spring ${year}`;
	}

	if (month <= 7 && day < 25) {
		return `Summer ${year}`;
	}

	return `Fall ${year}`;
}

export default {
	defaultTermName: getDefaultTermName(),
	gpa: {
		decimalPoints: 1,
		thresholds: [null, 5, 4, 3, 2],
	},
	grade: [
		{
			key: 'name',
			label: 'Class name',
			type: 'text',
		},
		{
			default: 4,
			key: 'credits',
			label: 'Credit units',
			type: 'number',
			width: 'medium',
		},
		{
			key: 'grade',
			label: 'Grade',
			mapping: { A: 5, B: 4, C: 3, D: 2, F: 0, O: 0 },
			type: 'enum',
			values: ['A', 'B', 'C', 'D', 'F', 'O'],
			width: 'medium',
		},
	],
	name: 'MIT',
};
